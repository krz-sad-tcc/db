const CHESS_OPENINGS_PATH = 'chess-openings-db/chess-openings.json';
const ROOT_FEN = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';
const ROOT_NAME = 'Initial Position';
const LICHESS_EXPLORER_BASE_URL = 'https://explorer.lichess.ovh/master?fen=';

module.exports = {
  CHESS_OPENINGS_PATH,
  ROOT_FEN,
  ROOT_NAME,
  LICHESS_EXPLORER_BASE_URL,
};
