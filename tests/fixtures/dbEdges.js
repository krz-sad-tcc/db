const dbEdge0 = {
  _id: 'leads_to/2320363',
  _rev: '_ejp6Vdy-_B',
  _key: '2320363',
  _from: 'positions/2319948',
  _to: 'positions/2320361',
  nGames: 416323,
  moveSan: 'Nf3',
};

const dbEdges = [dbEdge0];

module.exports = { dbEdges };
