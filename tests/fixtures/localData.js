const localData = {
  'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -': {
    name: 'Sicilian Defense',
    eco: 'B20',
    pgn: '1. e4 c5',
    uci: 'e2e4 c7c5',
    epd: 'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -',
  },
  'rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq -': {
    name: 'Sicilian Defense',
    eco: 'B27',
    pgn: '1. e4 c5 2. Nf3',
    uci: 'e2e4 c7c5 g1f3',
    epd: 'rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq -',
  },
};

module.exports = { localData };
