const { dbVertices } = require('./fixtures/dbVertices');
const { localData } = require('./fixtures/localData');
const { onlineData } = require('./fixtures/onlineData');
const { vertices } = require('./fixtures/vertices');
const {
  buildEdge,
  buildVertex,
  calculateNumberOfGames,
  toEpd,
  toUrlEncoding,
} = require('../src/utils');

const factory = (element, overwrite = {}) => {
  return {
    ...element,
    ...overwrite,
  };
};

global.console.log = jest.fn();

describe('#buildEdge', () => {
  test('builds an edge given two vertices', async () => {
    const v0 =
      dbVertices['rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -'];
    const v1 =
      dbVertices['rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq -'];
    const move =
      onlineData['rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -']
        .moves[0];
    const expectedEdge = {
      _from: 'positions/2319948',
      _to: 'positions/2320361',
      nGames: 416323,
      moveSan: 'Nf3',
    };
    expect(buildEdge(v0, v1, move)).toMatchObject(expectedEdge);
  });
});

describe('#buildVertex', () => {
  test('builds a vertex when local data exists', async () => {
    const expectedVertex =
      vertices['rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -'];
    expect(
      buildVertex(
        'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq c6 0 2',
        onlineData['rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -'],
        localData['rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -'],
      ),
    ).toStrictEqual(expectedVertex);
  });

  test('builds a vertex when local data is undefined', async () => {
    const expectedVertex = factory(
      vertices['rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -'],
      { eco: '', name: '' },
    );
    expect(
      buildVertex(
        'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq c6 0 2',
        onlineData['rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -'],
        undefined,
      ),
    ).toStrictEqual(expectedVertex);
  });
});

describe('#calculateNumberOfGames', () => {
  test('calculates number of games when the data has no zeroes', () => {
    expect(
      calculateNumberOfGames(
        onlineData['rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -'],
      ),
    ).toBe(506921);
  });

  test('calculates number of games when the data has zeroes', () => {
    expect(
      calculateNumberOfGames(
        factory(
          onlineData[
            'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -'
          ],
          { white: 0 },
        ),
      ),
    ).toBe(343800);
    expect(
      calculateNumberOfGames(
        factory(
          onlineData[
            'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -'
          ],
          { draws: 0 },
        ),
      ),
    ).toBe(296176);
    expect(
      calculateNumberOfGames(
        factory(
          onlineData[
            'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -'
          ],
          { black: 0 },
        ),
      ),
    ).toBe(373866);
    expect(
      calculateNumberOfGames(factory(onlineData['zeroGamesAndZeroChildren'])),
    ).toBe(0);
  });
});

describe('#toEpd', () => {
  test('generates EPD from FEN when there is no en passant target', () => {
    expect(
      toEpd('rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq - 0 2'),
    ).toEqual('rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -');
  });

  test('generates EPD from FEN when there is a legal en passant target', () => {
    expect(
      toEpd('r1bqkbnr/p4ppp/2n5/1pP5/N2p4/5N2/PP2PPPP/R1BQKB1R w KQkq b6 0 8'),
    ).toEqual('r1bqkbnr/p4ppp/2n5/1pP5/N2p4/5N2/PP2PPPP/R1BQKB1R w KQkq b6');
  });

  test('generates EPD from FEN when there is an illegal en passant target', () => {
    expect(
      toEpd('rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1'),
    ).toEqual('rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq -');
  });
});

describe('#toUrlEncoding', () => {
  test('substitutes %20 for spaces', () => {
    expect(toUrlEncoding(' ')).toEqual('%20');
  });

  test('substitutes %2F for slashes', () => {
    expect(toUrlEncoding('/')).toEqual('%2F');
  });

  test("doesn't change a string without spaces and slashes", () => {
    const s = '%20GenericString%2F';
    expect(toUrlEncoding(s)).toEqual(s);
  });

  test('encodes a complete fen', () => {
    expect(
      toUrlEncoding('rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'),
    ).toEqual(
      'rnbqkbnr%2Fpppppppp%2F8%2F8%2F8%2F8%2FPPPPPPPP%2FRNBQKBNR%20w%20KQkq%20-%200%201',
    );
  });
});
