const { onlineData } = require('./fixtures/onlineData');
const { ROOT_FEN } = require('../src/const');
const { addRoot, expandGraph } = require('../src/graph');
const utils = require('../src/utils');
const {
  connection,
  dropDb,
  query,
  setupDb,
  COLL_LEADS_TO,
  COLL_POSITIONS,
} = require('@krz-sad-tcc/db-tools');
const { vertices } = require('./fixtures/vertices');
const { addVertex, getVertexByEpd } = require('../src/db');

const factory = (element, overwrite = {}) => {
  return {
    ...element,
    ...overwrite,
  };
};

const getAllEdges = async (db) => {
  let dbEdges = await query(db, {
    query: `
      FOR e IN @@collection
        RETURN e
    `,
    bindVars: {
      '@collection': COLL_LEADS_TO,
    },
  });
  return dbEdges;
};

const getAllVertices = async (db) => {
  let dbVertices = await query(db, {
    query: `
      FOR v IN @@collection
        RETURN v
    `,
    bindVars: {
      '@collection': COLL_POSITIONS,
    },
  });
  return dbVertices;
};

jest.mock('../src/utils', () => {
  const actualModule = jest.requireActual('../src/utils');
  return {
    ...actualModule,
    fetchPositionData: jest.fn(),
  };
});

global.console.log = jest.fn();

describe('#addRoot', () => {
  const testDbName = 'addRoot';

  beforeEach(async () => {
    await setupDb(
      connection,
      testDbName,
      process.env.DB_USER,
      process.env.DB_PASS,
    );
  });

  afterEach(async () => {
    await dropDb(connection, testDbName);
  });

  const db = connection.database(testDbName);

  test('given an empty graph, it adds the root', async () => {
    utils.fetchPositionData.mockReturnValue(onlineData['initial']);

    await addRoot(db, 0);

    const vertices = await getAllVertices(db);
    expect(vertices.map((v) => v.fen)).toContain(ROOT_FEN);
  });
});

describe('#expandGraph', () => {
  const testDbName = 'expandGraph';

  beforeEach(async () => {
    await setupDb(
      connection,
      testDbName,
      process.env.DB_USER,
      process.env.DB_PASS,
    );
  });

  afterEach(async () => {
    await dropDb(connection, testDbName);
  });

  const db = connection.database(testDbName);

  test("given a graph with no leaves, it doesn't do anything", async () => {
    // Avoid making API calls even if expandGraph breaks.
    utils.fetchPositionData.mockReturnValue(
      onlineData['zeroGamesAndZeroChildren'],
    );

    await expandGraph(db, 0, 0);

    const dbVertices = await getAllVertices(db);
    expect(dbVertices).toHaveLength(0);
  });

  test('given a graph with leaves, it expands only the ones with nGames ≥ minGames', async () => {
    const epd0 = 'rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq -';
    const epd1 = 'rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq -';

    utils.fetchPositionData
      .mockReturnValueOnce(onlineData[epd1])
      .mockReturnValue(onlineData['zeroGamesAndZeroChildren']);

    const minGames = 1000;
    const v0 = factory(vertices[epd0], { nGames: minGames - 1 });
    const v1 = factory(vertices[epd1], { nGames: minGames });

    await addVertex(db, v0);
    await addVertex(db, v1);

    let dbVertex0 = await getVertexByEpd(db, epd0);
    let dbVertex1 = await getVertexByEpd(db, epd1);
    let dbVertices = await getAllVertices(db);
    expect(dbVertex0.isLeaf).toBe(true);
    expect(dbVertex1.isLeaf).toBe(true);
    expect(dbVertices).toHaveLength(2);

    await expandGraph(db, minGames, 0);

    dbVertex0 = await getVertexByEpd(db, epd0);
    dbVertex1 = await getVertexByEpd(db, epd1);
    dbVertices = await getAllVertices(db);
    expect(dbVertex0.isLeaf).toBe(true);
    expect(dbVertex1.isLeaf).toBe(false);
    expect(dbVertices).toHaveLength(2 + onlineData[epd1].moves.length);
  });

  test('given a graph with two leaves that when expanded share a child, it adds two edges and one vertex', async () => {
    const v0 = vertices['initial'];
    const v1 = vertices['initial'];
    const commonMove = onlineData['initial'].moves[0];
    const epdAfterMove =
      'rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq -';

    utils.fetchPositionData.mockReturnValue(
      factory(onlineData['zeroGamesAndZeroChildren'], { moves: [commonMove] }),
    );

    const dbVertex0 = await addVertex(db, v0);
    const dbVertex1 = await addVertex(db, v1);

    let dbEdges = await getAllEdges(db);
    expect(dbEdges).toHaveLength(0);

    let dbVertices = await getAllVertices(db);
    expect(dbVertices).toHaveLength(2);

    await expandGraph(db, 1, 0);

    const newVertex = await getVertexByEpd(db, epdAfterMove);

    dbEdges = await getAllEdges(db);
    expect(dbEdges).toHaveLength(2);
    expect(dbEdges).toContainEqual(
      expect.objectContaining({
        _from: dbVertex0._id,
        _to: newVertex._id,
      }),
    );
    expect(dbEdges).toContainEqual(
      expect.objectContaining({
        _from: dbVertex1._id,
        _to: newVertex._id,
      }),
    );

    dbVertices = await getAllVertices(db);
    expect(dbVertices).toHaveLength(3);
  });
});
